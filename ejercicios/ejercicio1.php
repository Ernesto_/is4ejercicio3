<!DOCTYPE html>
<html>
<head>
    <title>Script PHP en HTML</title>
</head>
<body>
    <?php
    // Crear una variable que almacene su nombre y apellido.
    $nombreApellido = "Ernesto Benitez";

    // Crear una variable que guarde su nacionalidad.
    $nacionalidad = "Paraguaya";

    // Usar la función echo para imprimir en pantalla el contenido de la variable que almacena su nombre y apellido.
    echo '<p style="color: red; font-weight: bold;">' . $nombreApellido . '</p>';

    // Usar la función print para imprimir en pantalla el contenido de la variable que almacena su nacionalidad.
    echo '<p style="text-decoration: underline;">' . $nacionalidad . '</p>';
    ?>
</body>
</html>