<!DOCTYPE html>
<html>
<head>
  <title>Tabla de Multiplicar del 9</title>
  <style>
    /* Estilo para las filas impares (gris) */
    .impar {
      background-color: #ddd;
    }
    /* Estilo para las filas pares (blanco) */
    .par {
      background-color: #fff;
    }
  </style>
</head>
<body>
  <table>
    <thead>
      <tr>
        <th>Multiplicando</th>
        <th>Resultado</th>
      </tr>
    </thead>
    <tbody>
      <?php
      // Iterar para generar la tabla de multiplicar del 9
      for ($i = 1; $i <= 10; $i++) {
        $resultado = 9 * $i;
        // Determinar la clase CSS para la fila (impar o par)
        $claseFila = ($i % 2 == 0) ? "par" : "impar";
        echo '<tr class="' . $claseFila . '">';
        echo '<td>9 x ' . $i . '</td>';
        echo '<td>' . $resultado . '</td>';
        echo '</tr>';
      }
      ?>
    </tbody>
  </table>
</body>
</html>