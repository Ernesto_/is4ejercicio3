<?php
// Función para generar un número aleatorio par en el rango especificado
function generarNumeroParAleatorio($min, $max) {
  $numero = rand($min, $max);
  // Asegurarse de que el número sea par
  if ($numero % 2 !== 0) {
    $numero += 1;
  }
  return $numero;
}

// Imprimir 900 números aleatorios pares
for ($i = 1; $i <= 900; $i++) {
  $numero = generarNumeroParAleatorio(1, 10000);
  echo $numero;
  if ($i < 900) {
    echo ', '; // Agregar coma y espacio entre los números (excepto el último)
  }
}
?>