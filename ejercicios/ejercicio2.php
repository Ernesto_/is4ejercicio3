<!DOCTYPE html>
<html>
<head>
 <title>Concatenación de Cadenas en PHP</title>
</head>
<body>
 <?php
 //Definir dos cadenas en variables diferentes
 $cadena1 = "Hola, ";
 $cadena2 = "mundo!";

 // Concatenar las dos cadenas utilizando el operador punto (.)
$resultado = $cadena1 . $cadena2;  

// Imprimir el resultado
echo '<p>' . $resultado . '</p>';  
 ?>
</body>
</html>