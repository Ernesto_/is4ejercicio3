<!DOCTYPE html>
<html>
<head>
  <title>Manipulación de Variables</title>
</head>
<body>
  <?php
  // Crear una variable con el contenido 24.5
  $variable = 24.5;

  // Determinar y imprimir el tipo de datos de la variable original
  echo "Tipo de datos de la variable original: " . gettype($variable) . "<br>";

  // Modificar el contenido de la variable a "HOLA"
  $variable = "HOLA";

  // Determinar y imprimir el tipo de datos de la variable modificada
  echo "Tipo de datos de la variable modificada: " . gettype($variable) . "<br>";

  // Setear el tipo de la variable modificada a un tipo entero
  settype($variable, "integer");

  // Determinar y mostrar el contenido y tipo de la variable con var_dump
  echo "Contenido y tipo de la variable después de setearla a tipo entero: ";
  var_dump($variable);
  ?>
</body>
</html>