<?php
// Generar valores aleatorios para las notas parciales y final
$parcial1 = rand(0, 30);
$parcial2 = rand(0, 20);
$final1 = rand(0, 50);

// Calcular la nota acumulada sumando las tres notas
$nota_acumulada = $parcial1 + $parcial2 + $final1;

// Determinar la nota final basada en la nota acumulada usando switch
switch (true) {
  case ($nota_acumulada >= 0 && $nota_acumulada <= 59):
    $nota_final = "Nota 1";
    break;
  case ($nota_acumulada >= 60 && $nota_acumulada <= 69):
    $nota_final = "Nota 2";
    break;
  case ($nota_acumulada >= 70 && $nota_acumulada <= 79):
    $nota_final = "Nota 3";
    break;
  case ($nota_acumulada >= 80 && $nota_acumulada <= 89):
    $nota_final = "Nota 4";
    break;
  case ($nota_acumulada >= 90 && $nota_acumulada <= 100):
    $nota_final = "Nota 5";
    break;
  default:
    $nota_final = "Fuera de rango";
    break;
}

// Imprimir la nota final
echo "Nota final del alumno: " . $nota_final;
?>