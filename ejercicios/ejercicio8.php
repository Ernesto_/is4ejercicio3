<?php
// Generar números aleatorios hasta que se encuentre un número divisible por 983
while (true) {
  $numero = rand(1, 1000000); // Generar un número aleatorio en un rango adecuado

  // Verificar si el número es divisible por 983
  if ($numero % 983 == 0) {
    echo "Se generó un número divisible por 983: $numero";
    break; // Salir del ciclo
  }
}
?>