<?php
// Generar valores enteros aleatorios para $a, $b y $c entre 99 y 999
$a = rand(99, 999);
$b = rand(99, 999);
$c = rand(99, 999);

// Evaluar la expresión $a*3 > $b+$c utilizando el operador ternario
$resultado = ($a * 3 > $b + $c) ? "La expresión \$a*3 es mayor que la expresión \$b+\$c" : "La expresión \$b+\$c es mayor o igual que la expresión \$a*3";

// Imprimir el resultado
echo $resultado;
?>